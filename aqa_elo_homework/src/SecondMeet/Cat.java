package SecondMeet;

public class Cat extends Animal{
    Cat(String name, String color) {
        super (name, color);

    }

    String className = this.getClass().getSimpleName();

    public void voice () {
        System.out.println("I am a " + className + ", my name is " + getName() + ", my color is " + getColor());
    }

}