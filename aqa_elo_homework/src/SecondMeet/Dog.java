package SecondMeet;

public class Dog extends Animal{
    Dog(String name, String color) {
        super (name, color);
    }

    String className = this.getClass().getSimpleName();

    public void voice () {
        System.out.println("I am a " + className + ", my name is " + getName() + ", my color is " + getColor());
    }
}
