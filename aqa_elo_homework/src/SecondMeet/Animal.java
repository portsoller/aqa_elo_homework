package SecondMeet;

public class Animal {
    private String name;
    private String color;

    Animal (String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getColor() {
        return this.color = color;
    }

    public String getName() {
        return this.name = name;
    }

    public void voice() {
    }
}