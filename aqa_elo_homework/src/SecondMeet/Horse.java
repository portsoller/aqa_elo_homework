package SecondMeet;

public class Horse extends Animal{
    Horse(String name, String color) {
        super (name, color);
    }

    String className = this.getClass().getSimpleName();

    public void voice () {
        System.out.println("I am a " + className + ", my name is " + getName() + ", my color is " + getColor());
    }


}